import { SafeAreaView, StyleSheet, View } from "react-native";
import DoremonCard from "./components/DoremonCard";
import { ScrollView } from "react-native";
import Forms from "./components/Forms/Forms";
import SectionLists from "./components/SectionList/SectionLists";
import FlatLists from "./components/FlatList/FlatLists";
import RequestsAPI from "./components/RequestsAPI/RequestsAPI";
import Navigation from "./components/Navigation/Navigation";

export default function App() {
  const dataDoremon = {
    name: "PokemonCard",
    image: require("./assets/download.jpg"),
    type: "ccccccc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };
  const dataNobita = {
    name: "PokemonCard",
    image: require("./assets/Nobita.png"),
    type: "cc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };
  const dataChaen = {
    name: "PokemonCard",
    image: require("./assets/chen1.png"),
    type: "cc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };
  const dataShizuka = {
    name: "PokemonCard",
    image: require("./assets/Shizuka.png"),
    type: "cc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };
  const dataSeko = {
    name: "PokemonCard",
    image: require("./assets/Suneo.gif"),
    type: "cc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };
  const dataDoremo2 = {
    name: "PokemonCard ",
    image: require("./assets/doraemons2.png"),
    type: "cc",
    hp: 49,
    moves: ["cc,vv,vv"],
  };

  return (
    <Navigation />
    // <RequestsAPI />
    // <Forms />
    // <SectionLists />
    // <FlatLists />
    // <SafeAreaView style={styles.container}>
    //   <ScrollView>
    //     <View>
    //       <DoremonCard {...dataDoremon} />
    //       <DoremonCard {...dataNobita} />
    //       <DoremonCard {...dataChaen} />
    //       <DoremonCard {...dataShizuka} />
    //       <DoremonCard {...dataDoremo2} />
    //       <DoremonCard {...dataSeko} />
    //     </View>
    //   </ScrollView>
    // </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: { backgroundColor: "#cccc", flex: 1 },
  header: { backgroundColor: "black", color: "#fff", textAlign: "center" },
  footer: { backgroundColor: "black", color: "#fff", textAlign: "center" },
});
