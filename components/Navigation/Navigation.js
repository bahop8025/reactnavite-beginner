import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Pressable, StyleSheet, Text } from "react-native";
import HomeScreen from "../../screens/HomeScreen";
import AboutScreen from "../../screens/AboutScreen";

const Stack = createNativeStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer style={styles.container}>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerStyle: {
            backgroundColor: "red",
          },
          headerTintColor: "green",
          headerTitleStyle: { fontSize: 20 },
          headerRight: () => (
            <Pressable onPress={() => alert("Menu button")}>
              <Text style={{ color: "#fff" }}>Menu</Text>
            </Pressable>
          ),
          contentStyle: {
            backgroundColor: "black",
          },
        }}
      >
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "Hello",
          }}
        />
        <Stack.Screen
          name="About"
          component={AboutScreen}
          initialParams={{ name: "cccc" }}
          options={({ route }) => ({ title: route.params.name })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: { backgroundColor: "#cccc", flex: 1 },
});
