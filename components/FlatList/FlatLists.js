import { FlatList, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { data } from "../../data";

export default function FlatLists() {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <FlatList
          data={data}
          renderItem={({ item }) => {
            return (
              <View>
                <Text>{item.title}</Text>
              </View>
            );
          }}
          keyExtractor={(item) => {
            return item.id.toString();
          }}
          ItemSeparatorComponent={<View style={{ height: 40 }} />}
          ListEmptyComponent={<Text>No item</Text>}
          ListHeaderComponent={<Text style={styles.header}>Header</Text>}
          ListFooterComponent={<Text style={styles.footer}>Footer</Text>}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: { backgroundColor: "#cccc", flex: 1 },
  header: { backgroundColor: "black", color: "#fff", textAlign: "center" },
  footer: { backgroundColor: "black", color: "#fff", textAlign: "center" },
});
