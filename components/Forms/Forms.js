import { useState } from "react";
import {
  Button,
  Image,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";

export default function Forms() {
  const [userName, setUserName] = useState("");
  const [passwork, setPasswork] = useState("");
  const [error, setError] = useState({});

  const validateForm = () => {
    let errors = {};

    if (!userName) errors.userName = "User name is required";
    if (!passwork) errors.passwork = "Passwork is required";

    return Object.keys(errors).length === 0;
  };

  const hadleSubmit = () => {
    if (validateForm()) {
      console.log(userName, "ccc");
      setUserName("");
      setPasswork("");
      setError({});
    }
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={100}
      style={styles.container}
    >
      <View style={styles.form}>
        <Image source={require("../../assets/chen1.png")} />
        <Text>User Name</Text>
        <TextInput
          style={styles.input}
          placeholder="User name"
          value={userName}
          onChangeText={setUserName}
        />
        {error.userName ? (
          <Text style={styles.error}>{error.userName}</Text>
        ) : null}
        <Text>Passwork</Text>
        <TextInput
          style={styles.input}
          placeholder="Passwork"
          secureTextEntry
          value={passwork}
          onChangeText={setPasswork}
        />
        {error.passwork ? (
          <Text style={styles.error}>{error.passwork}</Text>
        ) : null}
        <Button title="Login" onPress={hadleSubmit} />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontalL: 20,
    margin: 20,
    backgroundColor: "#ccccc",
  },
  form: {
    backgroundColor: "#fff",
    padding: 20,
    borderRadius: 10,
    shadowColor: "black",
    shadowOffset: {
      widthL: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 4,
  },
  input: {
    height: 40,
    borderColor: "#ddd",
    borderWidth: 1,
    marginBottom: 15,
    padding: 10,
    borderRadius: 5,
  },
  error: {
    color: "red",
    paddingBottom: 10,
  },
});
