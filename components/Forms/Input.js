import { useState } from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TextInput,
  View,
} from "react-native";

export default function Input() {
  const [input, setInput] = useState("");
  const [isDarkMode, setIsDarkMode] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.input}
        value={input}
        onChangeText={setInput}
        // secureTextEntry
        // keyboardType="numeric"
      />
      <TextInput style={[styles.input, styles.multiline]} multiline />
      <Text style={styles.text}>Text</Text>
      <View>
        <Switch
          value={isDarkMode}
          onValueChange={() => setIsDarkMode((pre) => !pre)}
          trackColor={{ false: "black", true: "blue" }}
          thumbColor="red"
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "red",
    flex: 1,
    padding: StatusBar.currentHeight,
  },
  input: { height: 70, borderWidth: 1, margin: 12, padding: 10 },
  text: { color: "red", fontSize: 30 },
  multiline: { minHeight: 100, textAlignVertical: "top" },
});
