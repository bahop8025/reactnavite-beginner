import {
  SafeAreaView,
  StyleSheet,
  Text,
  Platform,
  Image,
  View,
} from "react-native";

export default function DoremonCard({ name, image, type, hp, moves }) {
  return (
    <SafeAreaView>
      <View style={styles.card}>
        <View style={styles.container}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.hp}>{hp}</Text>
        </View>
        <Image
          style={styles.image}
          source={image}
          accessibilityLabel={`${name} doremon`}
          resizeMode="contain"
        />
        <View style={styles.description}>
          <Text style={styles.type}>{type}</Text>
          <Text>Movie:{moves.join(", ")}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    borderRadius: 16,
    borderWidth: 2,
    padding: 16,
    margin: 16,
    ...Platform.select({
      ios: {
        shadowOffset: { width: 2, height: 2 },
        shadowColor: "#333",
        shadowOpacity: 0.3,
        shadowRadius: 4,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  name: {
    fontSize: 30,
    fontWeight: "bold",
  },
  hp: {
    fontSize: 22,
  },
  image: {
    width: "100%",
    height: 200,
  },
  description: {
    alignItems: "center",
    marginTop: 10,
  },
  type: {
    fontSize: 20,
  },
});
