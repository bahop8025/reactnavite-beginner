import { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Button,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";

export default function RequestsAPI() {
  const [postLists, setPostLists] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [postTitle, setPostTitle] = useState("");
  const [postBody, setPostBody] = useState("");
  const [isPosting, setIsPosting] = useState(false);
  const [error, setError] = useState("");

  const fetchData = async (limit = 10) => {
    try {
      const res = await fetch(
        `https://jsonplaceholder.typicode.com/posts?_limit=${limit}`
      );
      const data = await res.json();

      setPostLists(data);
      setIsLoading(false);
      setError("");
    } catch (error) {
      console.error(error);
      setIsLoading(false);
      setError("error");
    }
  };

  const handleRefresh = () => {
    setRefresh(true);
    fetchData(20);
    setRefresh(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const addPost = async () => {
    setRefresh(true);
    try {
      const res = await fetch(`https://jsonplaceholder.typicode.com/posts`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title: postTitle,
          body: postBody,
        }),
      });
      const newPost = await res.json();
      setPostLists([newPost, ...postLists]);
      setPostTitle("");
      setPostBody("");
      setIsPosting(false);
      setRefresh(false);
      setError("");
    } catch (error) {
      console.error("error");
      setError("error");
    }
  };

  if (isLoading) {
    return (
      <SafeAreaView style={styles.loading}>
        <ActivityIndicator size="large" color="red" />
        <Text>Loading...</Text>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {error ? (
        <View>
          <Text>{error}</Text>
        </View>
      ) : (
        <>
          <View>
            <TextInput
              placeholder="Input"
              value={postTitle}
              onChangeText={setPostTitle}
              style={styles.input}
            />
            <TextInput
              placeholder="Input"
              value={postBody}
              onChangeText={setPostBody}
              style={styles.input}
            />
            <Button
              title={isPosting ? "Adding...." : "Add Post"}
              onPress={addPost}
              disabled={isPosting}
            />
          </View>
          <View>
            <FlatList
              data={postLists}
              renderItem={({ item }) => {
                console.log(item);
                return (
                  <View style={styles.list}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text>{item.body}</Text>
                  </View>
                );
              }}
              refreshing={refresh}
              onRefresh={handleRefresh}
            />
          </View>
        </>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: { backgroundColor: "#cccc", flex: 1 },
  loading: {
    flex: 1,
    backgroundColor: "#cccc",
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    borderWidth: 1,
    borderColor: "#000",
    height: 40,
    padding: 10,
    margin: 10,
  },
  list: {
    borderWidth: 1,
    margin: 10,
    padding: 10,
  },
  title: {
    color: "red",
    fontSize: 18,
    textAlign: "center",
  },
});
