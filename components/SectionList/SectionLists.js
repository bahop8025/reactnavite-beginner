import {
  SafeAreaView,
  SectionList,
  StyleSheet,
  Text,
  View,
} from "react-native";
import data1 from "../../data.json";

export default function SectionLists() {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <SectionList
          sections={data1}
          renderItem={({ item }) => {
            console.log(item);
            return (
              <View>
                <Text>{item}</Text>
              </View>
            );
          }}
          renderSectionHeader={({ section }) => {
            return <Text>{section.type}</Text>;
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: { backgroundColor: "#cccc", flex: 1 },
  header: { backgroundColor: "black", color: "#fff", textAlign: "center" },
  footer: { backgroundColor: "black", color: "#fff", textAlign: "center" },
});
